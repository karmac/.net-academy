﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EssenatialTools.Models
{
    public class MinimumDiscountHelper: IDiscountHelper
    {
        public decimal ApplyDiscount(decimal totalParam)
        {
            if (totalParam > 100)
            {
                return totalParam * 0.9M;
            } else if (totalParam >= 10 && totalParam <= 100)
            {
                return totalParam - 5;
            } else if (totalParam >= 0 && totalParam < 10)
            {
                return totalParam;
            } else
            {
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}