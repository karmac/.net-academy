﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EssenatialTools.Startup))]
namespace EssenatialTools
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
