﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EssenatialTools.Models;

namespace EssentialTools.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private IDiscountHelper getTestObject()
        {
            return new MinimumDiscountHelper();
        }

        [TestMethod]
        public void Discount_Above_100()
        {
            //arange:
            IDiscountHelper target = getTestObject();
            decimal total = 200;

            //act:
            var discountedTotal = target.ApplyDiscount(total);

            //assert:
            Assert.AreEqual(total * 0.9M, discountedTotal);
        }

        [TestMethod]
        public void Discount_Between_10_And_100()
        {
            //arrange:
            IDiscountHelper target = getTestObject();

            //act:
            decimal tenZlotyDiscount = target.ApplyDiscount(10);
            decimal HundredZlotyDiscount = target.ApplyDiscount(100);
            decimal FiftyZlotyDiscount = target.ApplyDiscount(50);

            //assert:
            Assert.AreEqual(5, tenZlotyDiscount, "10 zl discount is wrong");
            Assert.AreEqual(95, HundredZlotyDiscount, "100 zl discount is wrong");
            Assert.AreEqual(45, FiftyZlotyDiscount, "50 zl discont is wrong");
        }

        [TestMethod]
        public void Discount_Less_Than_10()
        {
            //arrange:
            IDiscountHelper target = getTestObject();

            //act:
            decimal discount0 = target.ApplyDiscount(0);
            decimal discount5 = target.ApplyDiscount(5);

            //assert:
            Assert.AreEqual(0, discount0);
            Assert.AreEqual(5, discount5);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Discount_Negative_Total()
        {
            //arrange:
            IDiscountHelper target = getTestObject();

            //act:
            target.ApplyDiscount(-1);
        }
    }
}
