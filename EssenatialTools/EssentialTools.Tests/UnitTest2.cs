﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EssenatialTools.Models;
using Moq;
using System.Linq;

namespace EssentialTools.Tests
{
    [TestClass]
    public class UnitTest2
    {
        private Product[] products =
        {
            new Product { Name = "Kayak", Category = "Watersports", Price = 275M },
            new Product { Name = "Lifejacket", Category = "Watersports", Price = 48.95M },
            new Product {Name = "Soccer Ball", Category = "Soccer", Price = 34.95M },
            new Product {Name = "Corner Flag", Category = "Soccer", Price = 19.50M }
        };

        [TestMethod]
        public void Sum_Products_Correctly()
        {
            Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();
            mock.Setup(m => m.ApplyDiscount(It.IsAny<decimal>()))
                .Returns<decimal>(total => total);
            var target = new LinqValueCalculator(mock.Object);

            //act:
            var result = target.ValueProducts(products);

            //assert:
            Assert.AreEqual(products.Sum(e => e.Price), result);
        }

        private Product[] createProduct(decimal value)
        {
            return new[] { new Product { Price = value } };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]

        public void Going_Through_Variable_Discounts()
        {
            //arrange:
            Mock<IDiscountHelper> mock = new Mock<IDiscountHelper>();
            mock.Setup(m => m.ApplyDiscount(It.IsAny<decimal>()))
                .Returns<decimal>(total => total);
            mock.Setup(m => m.ApplyDiscount(It.Is<decimal>(v => v == 0)))
                .Throws<ArgumentOutOfRangeException>();
            mock.Setup(m => m.ApplyDiscount(It.Is<decimal>(v => v > 100)))
                .Returns<decimal>(total => (total * 0.9M));
            mock.Setup(m => m.ApplyDiscount(It.IsInRange<decimal>(10, 100, Range.Inclusive)))
                .Returns<decimal>(total => total - 5);
            var target = new LinqValueCalculator(mock.Object);

            //act:
            decimal FiveZlDiscount = target.ValueProducts(createProduct(5));
            decimal TenZlDiscount = target.ValueProducts(createProduct(10));
            decimal FiftyZlZlDiscount = target.ValueProducts(createProduct(50));
            decimal HundredZlDiscount = target.ValueProducts(createProduct(100));
            decimal FiveHundredZlDiscount = target.ValueProducts(createProduct(500));

            //assert:
            Assert.AreEqual(5, FiveZlDiscount, "5zl fail");
            Assert.AreEqual(5, TenZlDiscount, "10zl fail");
            Assert.AreEqual(45, FiftyZlZlDiscount, "50zl fail");
            Assert.AreEqual(95, HundredZlDiscount, "100zl fail");
            Assert.AreEqual(450, FiveHundredZlDiscount, "500zl fail");
            target.ValueProducts(createProduct(0));
        }
    }
}
