﻿using System.Linq;
using SportsStore.Domain.cs.Entities;

namespace SportsStore.Domain.cs.Abstract
{
    public interface IProductRepository
    {
        IQueryable<Product> Products { get; }
    }
}
