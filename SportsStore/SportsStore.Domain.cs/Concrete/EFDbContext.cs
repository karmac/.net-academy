﻿using SportsStore.Domain.cs.Entities;
using System.Data.Entity;

namespace SportsStore.Domain.cs.Concrete
{
    class EFDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}
