﻿using SportsStore.Domain.cs.Entities;
using SportsStore.Domain.cs.Abstract;
using System.Linq;


namespace SportsStore.Domain.cs.Concrete
{
    public class EFProductRepository : IProductRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<Product> Products
        {
            get {
                return context.Products;
            }
        }
    }
}
