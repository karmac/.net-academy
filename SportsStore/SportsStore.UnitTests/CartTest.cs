﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SportsStore.Domain.cs.Entities;
using System.Linq;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class CartTests
    {
        [TestMethod]
        public void Can_Add_New_Lines()
        {
            //Arrange - create some test products:
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            //Arrange - create a new cart:
            Cart target = new Cart();

            //Act:
            target.AddItem(p1, 1);
            target.AddItem(p2, 2);
            CartLine[] results = target.Lines.ToArray();

            //Assert:
            Assert.AreEqual(results[0].Product, p1);
            Assert.AreEqual(results[1].Product, p2);
        }

        [TestMethod]

        public void Can_Add_Quantity_For_Existing_Lines()
        {
            //Arrange:
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };

            //Arrange - new cart:
            Cart target = new Cart();

            //Act:
            target.AddItem(p1, 1);
            target.AddItem(p2, 1);
            target.AddItem(p1, 10);
            CartLine[] results = (target.Lines.OrderBy(c => c.Product.ProductID)).ToArray();

            //Assert:
            Assert.AreEqual(results.Length, 2);
            Assert.AreEqual(results[0].Quantity, 11);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]

        public void Can_Remove_Line()
        {
            //Arrange:
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };
            Product p3 = new Product { ProductID = 3, Name = "P3" };

            //Arrange - new cart:
            Cart target = new Cart();

            //Arrange - add some items first in order to being able to remove them:
            target.AddItem(p1, 2);
            target.AddItem(p2, 10);
            target.AddItem(p3, 3);

            //Act:
            target.RemoveLine(p1);

            //Assert:
            Assert.AreEqual((target.Lines.Where(c => c.Product == p1).Count()), 0);
            Assert.AreEqual(target.Lines.Count(), 2);
        }

        [TestMethod]

        public void Calcualte_Cart_Total()
        {
            //Arrange:
            Product p1 = new Product { ProductID = 1, Name = "P1", Price = 100M };
            Product p2 = new Product { ProductID = 2, Name = "P2", Price = 100M };

            //Arrange: 
            Cart target = new Cart();

            //Act:
            target.AddItem(p1, 2);
            target.AddItem(p2, 3);
            target.AddItem(p1, 4);
            decimal result = target.ComputeTotalValue();

            //Assert:
            Assert.AreEqual(result, 900M);            
        }

        [TestMethod]
        public void Can_Clear_Content()
        {
            //Arrange:            
            Product p1 = new Product { ProductID = 1, Name = "P1" };
            Product p2 = new Product { ProductID = 2, Name = "P2" };


            //Arrange:
            Cart target = new Cart();

            //Arrange - Add some items:
            target.AddItem(p1, 2);
            target.AddItem(p2, 3);

            //Act:
            target.Clear();

            //Assert:
            Assert.AreEqual(target.Lines.Count(), 0);            
        }
    }
}






