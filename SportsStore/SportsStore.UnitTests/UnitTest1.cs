﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using SportsStore.Domain.cs.Abstract;
using SportsStore.Domain.cs.Entities;
using SportsStore.WebUI.Models;
using Moq;
using System.Collections.Generic;
using System.Web.Mvc;
using SportsStore.WebUI.Controllers;
using SportsStore.WebUI.HtmlHelpers;

namespace SportsStore.UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Can_Paginate()
        {
            //Arrange:
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product {ProductID = 1, Name = "P1" },
                new Product {ProductID = 2, Name = "P2" },
                new Product {ProductID = 3, Name = "P3" },
                new Product {ProductID = 4, Name = "P4" },
                new Product {ProductID = 5, Name = "P5" }
            }.AsQueryable());

            ProductController controller = new ProductController(mock.Object);

            controller.PageSize = 3;

            //Act:
            ProductsListViewModel result = (ProductsListViewModel)controller.List(null, 2).Model;

            //Assert:
            Product[] prodArray = result.Products.ToArray();
            Assert.IsTrue(prodArray.Length == 2);
            Assert.AreEqual(prodArray[0].Name, "P4");
            Assert.AreEqual(prodArray[1].Name, "P5");
        }

        [TestMethod]
        public void Can_Generate_Page_Links()
        {
            //Arrange - we have to define an HTml helper 
            //in order to apply an extension method:
            HtmlHelper myHelper = null;

            //Arrange - creating PagingInfo data:
            PagingInfo pagingInfo = new PagingInfo
            {
                CurrentPage = 2,
                TotalItems = 28,
                ItemsPerPage = 10                
            };

            //Arrange - set up the delegate uisng a lambda expression:
            Func<int, string> pageUrlDelegate = (i => ("Page" + i));

            //Act:
            MvcHtmlString result = myHelper.PageLinks(pagingInfo, pageUrlDelegate);

            //Assert:
            Assert.AreEqual(result.ToString(), @"<a href=""Page1"">1</a>" + @"<a class=""selected"" href=""Page2"">2</a>" + @"<a href=""Page3"">3</a>");
        }

        [TestMethod]

        public void Can_Send_Pagination_View_Model()
        {
            //Arrange:
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductID = 1, Name = "P1" },
                new Product {ProductID = 2, Name = "P2" },
                new Product {ProductID = 3, Name = "P3" },
                new Product {ProductID = 4, Name = "P4" },
                new Product {ProductID = 5, Name = "P5" }
            }.AsQueryable());

            //Arrange:
            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            //Act:
            ProductsListViewModel result = (ProductsListViewModel)controller.List(null, 2).Model;

            //Assert:
            PagingInfo pageInfo = result.PagingInfo;
            Assert.AreEqual(pageInfo.CurrentPage, 2);
            Assert.AreEqual(pageInfo.ItemsPerPage, 3);
            Assert.AreEqual(pageInfo.TotalItems, 5);
            Assert.AreEqual(pageInfo.TotalPages, 2);            
        }

        [TestMethod]

        public void Can_Filter_Category()
        {
            //Arrange
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[] {
                new Product { ProductID = 1, Name = "P1", Category = "cat1"},
                new Product { ProductID = 2, Name = "P2", Category = "cat2" },
                new Product { ProductID = 3, Name = "P3", Category = "cat1" },
                new Product { ProductID = 4, Name = "P4", Category = "cat2" },
                new Product { ProductID = 5, Name = "P5", Category = "cat3" }
            }.AsQueryable);

            //Arrange:
            ProductController controller = new ProductController(mock.Object);
            controller.PageSize = 3;

            //Act:
            Product[] result = ((ProductsListViewModel)controller.List("cat2", 1).Model)
                .Products.ToArray();

            //Assert:
            Assert.AreEqual(result.Length, 2);
            Assert.IsTrue(result[0].Name == "P2" && result[0].Category == "cat2");
            Assert.IsTrue(result[1].Name == "P4" && result[1].Category == "cat2");
        }

        [TestMethod]

        public void Can_Create_Categories()
        {
            //Arrange:
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductID = 1, Name = "P1", Category = "Apples" },
                new Product {ProductID = 2, Name = "P2", Category = "Apples" },
                new Product {ProductID = 3, Name = "P3", Category = "Oranges" },
                new Product {ProductID = 4, Name = "P4", Category = "Plums" }
            }.AsQueryable());

            //Arrange - create the controller:
            NavController target = new NavController(mock.Object);

            //Act - get the set of categories:
            string[] results = ((IEnumerable<string>)target.Menu().Model).ToArray();

            //Assert:
            Assert.AreEqual(results.Length, 3);
            Assert.AreEqual(results[0], "Apples");
            Assert.AreEqual(results[1], "Oranges");
            Assert.AreEqual(results[2], "Plums");
        }
        
        [TestMethod]           

        public void Indicates_Selected_Category()
        {
            //Arrange - creating a mock repository:
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductID = 1, Name = "P1", Category = "Apples" },
                new Product {ProductID = 2, Name = "P2", Category = "Oranges" }
            }.AsQueryable());

            //Arrange - creating a controller:
            NavController target = new NavController(mock.Object);

            //Arrange - define the category to select:
            string categoryToSelect = "Apples";

            //Act:
            string result = target.Menu(categoryToSelect).ViewBag.SelectedCategory;

            //Assert:
            Assert.AreEqual(result, categoryToSelect);
        }

        [TestMethod]

        public void Generate_Category_Specific_Product_Count()
        {
            //Arrange - create a mock repo:
            Mock<IProductRepository> mock = new Mock<IProductRepository>();
            mock.Setup(m => m.Products).Returns(new Product[]
            {
                new Product {ProductID = 1, Name = "P1", Category = "Apples" },
                new Product {ProductID = 2, Name = "P2", Category = "Oranges" },
                new Product {ProductID = 3, Name = "P3", Category = "Apples" },
                new Product {ProductID = 4, Name = "P4", Category = "Oranges" },
                new Product {ProductID = 5, Name = "P5", Category = "Plums" },
            }.AsQueryable());

            //Arrange - create a controller and make page size 3 items:
            ProductController target = new ProductController(mock.Object);
            target.PageSize = 3;

            //Action - test the product counts for different categories:
            int res1 = ((ProductsListViewModel)target.List("Apples").Model)
                .PagingInfo.TotalItems;
            int res2 = ((ProductsListViewModel)target.List("Oranges").Model)
                .PagingInfo.TotalItems;
            int res3 = ((ProductsListViewModel)target.List("Plums").Model)
                .PagingInfo.TotalItems;
            int resAll = ((ProductsListViewModel)target.List(null).Model)
                .PagingInfo.TotalItems;

            //Assert:
            Assert.AreEqual(res1, 2);
            Assert.AreEqual(res2, 2);
            Assert.AreEqual(res3, 1);
            Assert.AreEqual(resAll, 5);
        }        
    }
}










