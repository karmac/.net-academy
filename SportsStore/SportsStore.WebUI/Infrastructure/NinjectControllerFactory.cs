﻿using System;
using System.Web.Routing;
using System.Web.Mvc;
using Ninject;
using SportsStore.Domain.cs.Entities;
using SportsStore.Domain.cs.Abstract;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SportsStore.Domain.cs.Concrete;

namespace SportsStore.WebUI.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext
            requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                return null;
            }
            else
            {
                return (IController)ninjectKernel.Get(controllerType);
            }
        }

        private void AddBindings()
        {  
            ninjectKernel.Bind<IProductRepository>().To<EFProductRepository>();
        }

    }
}